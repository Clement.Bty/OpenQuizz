//
//  Question.swift
//  OpenQuizz
//
//  Created by Clément Bty on 08/04/2018.
//  Copyright © 2018 Clément Bty. All rights reserved.
//
import Foundation

struct Question{
    var title: String
    var isCorrect: Bool
}
