//
//  ViewController.swift
//  OpenQuizz
//
//  Created by Clément Bty on 07/04/2018.
//  Copyright © 2018 Clément Bty. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var questionView: QuestionView!
    @IBOutlet weak var newGameButton: UIButton!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var labelRep: UILabel!
    
    var screenWidth = UIScreen.main.bounds.width
    
    /*** Model ***/
    var game = Game()
    
    
    override func viewDidLoad() {        
        super.viewDidLoad()
        let name = Notification.Name(rawValue: "QuestionsLoaded")
        NotificationCenter.default.addObserver(self, selector: #selector(questionsLoaded), name: name, object: nil)
        
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(dragQuestionView(_:)))
        questionView.addGestureRecognizer(panGestureRecognizer)
        
        newGameButton.layer.cornerRadius = 5
        newGameButton.layer.shadowColor = #colorLiteral(red: 0.3333333333, green: 0.8196078431, blue: 0.3411764706, alpha: 1)
        newGameButton.layer.shadowOpacity = 0.5
        newGameButton.layer.shadowOffset = CGSize(width: 0, height: 5)
        newGameButton.layer.shadowRadius = 6
    }
    
    
    /*** Actions ***/
    @IBAction func didTapNewGameButton() {
        
    }
    
    @IBAction func touch(_ sender: UIButton) {
        UIButton.animate(withDuration: 0.15,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                
                            }, completion: { finish in
                                self.startNewGame()
                            })
        })
    }
    
   
    
    /*** Methodes ***/
    private func startNewGame(){
        newGameButton.isHidden = true
        activityIndicator.isHidden = false
        questionView.title = "Loading ..."
        questionView.style = .standart
        scoreLabel.text = "0 / 10"
        game.refresh()
    }
    
    @objc public func questionsLoaded(){
        
        activityIndicator.isHidden = true
        newGameButton.isHidden = false
        questionView.title = game.currentQuestion.title
        
    }
    
    @objc public func dragQuestionView(_ sender: UIPanGestureRecognizer){
        
        if game.state == .ongoing {
            switch sender.state{
            case .began, .changed:
                transformQuestionViewWith(gesture: sender)
            case .cancelled, .ended:
                answerQuestion()
            default:
                break
            }
        }
        
        
    }
    
    private func transformQuestionViewWith(gesture: UIPanGestureRecognizer){
        let translation = gesture.translation(in: questionView)
        let translationTransform = CGAffineTransform(translationX: translation.x, y: translation.y)
        
        let translationPercent = translation.x/(screenWidth/2)
        
        let rotationAngle = (CGFloat.pi / 4)*translationPercent
        let rotationTransform = CGAffineTransform(rotationAngle: rotationAngle)
        
        let transformDouble = translationTransform.concatenating(rotationTransform)
        questionView.transform = transformDouble
        
        if(translation.x > 0){
            questionView.style = .correct
        }
        else{
            questionView.style = .incorrect
        }
    }
    
    private func answerQuestion(){
        switch questionView.style {
        case .correct:
            game.answerCurrentQuestion(with: true)
        case .incorrect:
            game.answerCurrentQuestion(with: false)
        case .standart:
            break
        }
        
        scoreLabel.text = "\(game.score) / 10"
        
        
        
        var translationTransform: CGAffineTransform // Envoie les cartes vers la droite ou vers la gauche
        
        
        if questionView.style == .correct{
            translationTransform = CGAffineTransform(translationX: 1.5*screenWidth, y: 100)
        }
        else{
            translationTransform = CGAffineTransform(translationX: -1.5*screenWidth, y: 100)
        }
        
        UIView.animate(withDuration: 0.4, animations: {
            self.questionView.transform = translationTransform
        }) { (sucess) in
            if sucess {
                self.showQuestionView()
                self.showLabelRep()
            }
        }
       
        
    }
    private func showQuestionView(){
        questionView.transform = .identity
        questionView.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
        questionView.style = .standart
        
        if(game.isRight){
            labelRep.text = "+1 Point"
        }
        else{
            labelRep.text = "Faux !"
        }
        
        switch(game.state){
        case .ongoing:
            questionView.title = game.currentQuestion.title
        case .over:
            questionView.title = "Game Over look at your Score !"
        }
        
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: [], animations: {
            self.questionView.transform = .identity
            
        }, completion: nil)
        
    }
    
    private func showLabelRep(){
       
        UIView.animate(withDuration: 1.3, delay: 0, options: [.autoreverse, .repeat], animations: {
            UIView.setAnimationRepeatCount(1)
            self.labelRep.alpha = 1
        }) { (sucess) in
            self.labelRep.alpha = 0
        }
    }

   


}

