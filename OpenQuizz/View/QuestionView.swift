//
//  QuestionView.swift
//  OpenQuizz
//
//  Created by Clément Bty on 15/04/2018.
//  Copyright © 2018 Clément Bty. All rights reserved.
//

import UIKit

class QuestionView: UIView {
    
    enum Style{
        case correct
        case incorrect
        case standart
    }

    @IBOutlet private var label : UILabel!
    @IBOutlet private var icon: UIImageView!
    
    var title = " " {
        didSet{
            label.text = title
        }
    }
    
    var style: Style = .standart {
        didSet{
            setStyle(style)
        }
    }
    
    private func setStyle(_ style: Style){
        
        self.layer.cornerRadius = 5
        switch style{
        case .standart:
            self.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.label.textColor = #colorLiteral(red: 0.3179988265, green: 0.3179988265, blue: 0.3179988265, alpha: 1)
            icon.image = nil
        case .correct:
            self.backgroundColor = #colorLiteral(red: 0.2638437359, green: 0.8875912119, blue: 0.29093286, alpha: 1)
            self.label.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
            icon.image = UIImage(named: "Icon Correct")
        case .incorrect:
            self.backgroundColor = #colorLiteral(red: 0.6980392157, green: 0.03137254902, blue: 0.01960784314, alpha: 1)
            self.label.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            icon.image = #imageLiteral(resourceName: "Icon Error")
        }
    }
}
